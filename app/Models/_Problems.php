<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class _Problems extends Model
{
	protected $table = 'problems';
	protected $primaryKey = 'id';
	protected $fillable = array('account_id','name', 'description','status');

}
