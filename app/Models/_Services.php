<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class _Services extends Model
{
	protected $table = 'services';
	protected $primaryKey = 'id';
	

	function services( $select, $account_id ) {
		try {
			$paginate = isset($select['paginate'])?$select['paginate']:null;
			$first = isset($select['first'])?$select['first']:null;
			$get = isset($select['get'])?$select['get']:null;

			$select = $this->services_filter( $select );
			$query = DB::table('services as a')
				->select(DB::raw( 'a.id, a.service_id, a.customer_id, a.problem, a.device, a.description, a.cost, a.status,
				DATE_FORMAT(a.start_date, "%b %d, %Y") as start_date,
				DATE_FORMAT(a.start_date, "%Y-%m-%d") as start_date_format,
				DATE_FORMAT(a.close_date, "%b %d, %Y") as close_date,
				DATE_FORMAT(a.close_date, "%Y-%m-%d") as close_date_format,
				DATE_FORMAT(a.created_at, "%b %d, %Y %h:%i %p") as created_at,
				b.name, b.phone, c.name AS user'))
				->leftJoin('customers as b', 'a.customer_id', '=', 'b.id')
				->leftJoin('users as c', 'a.logged_by', '=', 'c.id')
				->whereRaw( "a.account_id = '".$account_id."' " . implode( ' ', $select ) )
				->orderby('a.start_date','DESC')
				->orderby('a.created_at','DESC');
			if(!$query){
				return false;
			}
		} catch(\Exception $e){
			return false;
		}

		if($paginate){
			return $query->paginate($paginate);
		} else if($first){
			return $query->first();
		} else if($get){
			return $query->get();
		}

		return $query;
	}

	function services_filter( $select ) {
		$data             = array();
		$data['service_id']   = isset( $select['service_id'] ) ? " AND a.service_id LIKE '%" . $select['service_id']."%'" : "";
		$data['details']   = isset( $select['details'] ) ? " AND (b.name LIKE '%".$select['details']."%' ||
		b.phone LIKE '%".$select['details']."%') " : "";
		if(isset( $select['date'] ) ){
			$date = format_search_date($select['date']);
			$data['date'] = " AND date(a.start_date) BETWEEN date('".$date[0]."') AND date('".$date[1]."')";
		}
		return $data;
	}

	function report_total($from, $to){

		/*
		select DATE_FORMAT(a.start_date, "%b %d") as day, count(*) AS total,
(select count(*) FROM services WHERE start_date = a.start_date and status = 'open') as status_open,
(select count(*) FROM services WHERE start_date = a.start_date and status = 'closed') as status_closed,
(select count(*) FROM services WHERE start_date = a.start_date and status = 'returned') as status_returned
from services a 
where date(a.start_date) BETWEEN date('2018-07-01') and date('2018-07-31')
group by a.start_date
*/

		$query = DB::table('services AS a')
		->select(DB::raw('DATE_FORMAT(a.start_date, "%b %d") as day, count(*) AS total,
		(select count(*) FROM services WHERE start_date = a.start_date and status = "open") as status_open,
		(select count(*) FROM services WHERE start_date = a.start_date and status = "closed") as status_closed,
		(select count(*) FROM services WHERE start_date = a.start_date and status = "Open Returned") as status_returned'))
		//->where('status', 'open')
		->whereBetween('a.start_date', [$from, $to])
		->groupby('a.start_date')
		->orderby('a.start_date','asc')
		->get(['day','total','status_open','status_closed','status_returned']);
		return $query;
	}

}
