<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class _Customers extends Model
{
	protected $table = 'customers';
	protected $primaryKey = 'id';
	protected $fillable = array('account_id','name','phone','status');

	function customers( $select, $account_id) {
		try {
			$paginate = isset($select['paginate'])?$select['paginate']:null;
			$first = isset($select['first'])?$select['first']:null;
			$get = isset($select['get'])?$select['get']:null;

			$select = $this->customers_filter( $select );
			$query = DB::table('customers as a')
				->select(DB::raw( 'a.id, a.name, a.phone, a.phone_alt, a.email, a.status,
				DATE_FORMAT(a.created_at, "%b %d, %Y %h:%i %p") as created_at'))
				->whereRaw( "a.account_id = '".$account_id."' " . implode( ' ', $select ) );
			if(!$query){
				return false;
			}
		} catch(\Exception $e){
			return false;
		}

		if($paginate){
			return $query->paginate($paginate);
		} else if($first){
			return $query->first();
		} else if($get){
			return $query->get();
		}

		return $query;
	}

	function customers_filter( $select ) {
		$data             = array();
		$data['details']   = isset( $select['details'] ) ? " AND (a.name LIKE '%".$select['details']."%' ||
		a.phone LIKE '%".$select['details']."%' || a.email LIKE '%".$select['details']."%') " : "";
		if(isset( $select['date'] ) ){
			$date = format_search_date($select['date']);
			$data['date'] = " AND date(created_at) BETWEEN date('".$date[0]."') AND date('".$date[1]."')";
		}
		return $data;
	}

}
