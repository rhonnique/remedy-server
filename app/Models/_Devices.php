<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class _Devices extends Model
{
	protected $table = 'devices';
	protected $primaryKey = 'id';
	protected $fillable = array('account_id','name', 'description','status');

}
