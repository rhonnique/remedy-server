<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class _Resources extends Model
{
	protected $table = 'resources';
	protected $primaryKey = 'id';
	

	function resources( $select) {
		try {
			$paginate = isset($select['paginate'])?$select['paginate']:null;
			$first = isset($select['first'])?$select['first']:null;
			$get = isset($select['get'])?$select['get']:null;

			$select = $this->resources_filter( $select );
			$query = DB::table('resources as a')
				->select(DB::raw( 'a.id, a.name, a.phone, a.status,
				IF(a.status = "1", "Active", "Inactive") as status_text,
				DATE_FORMAT(a.created_at, "%b %d, %Y %h:%i %p") as created_at,
				b.name AS designation'))
				->leftJoin('designations as b', 'a.designation_id', '=', 'b.id')
				->whereRaw( "a.id !='' " . implode( ' ', $select ) );
			if(!$query){
				return false;
			}
		} catch(\Exception $e){
			return false;
		}

		if($paginate){
			return $query->paginate($paginate);
		} else if($first){
			return $query->first();
		} else if($get){
			return $query->get();
		}

		return $query;
	}

	function resources_filter( $select ) {
		$data             = array();
		$data['customer_id']   = isset( $select['id'] ) ? " AND a.id = '" . $select['id']."'" : "";
		$data['name']   = isset( $select['name'] ) ? " AND a.name LIKE '%" . $select['name']."%'" : "";
		$data['phone']   = isset( $select['phone'] ) ? " AND a.phone LIKE '%" . $select['phone']."%'" : "";
		$data['phone_alt']   = isset( $select['phone_alt'] ) ? " AND a.phone_alt LIKE '%" . $select['phone_alt']."%'" : "";
		$data['email']   = isset( $select['email'] ) ? " AND a.email LIKE '%" . $select['email']."%'" : "";
		return $data;
	}

}
