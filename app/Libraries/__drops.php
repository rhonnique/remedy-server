<?php

namespace App\Libraries;

use DB;

class __drops
{
    public function routes()
    {
        $query = DB::table('routes')->orderby('id', 'ASC')->pluck('name', 'id')->all();
        return $query;
    }

    public function networks()
    {
        $query = DB::table('networks')->orderby('network_id', 'ASC')->pluck('name', 'network_id')->all();
        return $query;
    }

    public function cities($id, $ajax = null)
    {
        if ($ajax) {
            $query = DB::table('local_governments')->where('state_id', $id)
                ->orderby('name', 'ASC')->pluck('name', 'id')->all();
            return $query;
        } else {
            $list = '<option value="">- Select City -</option>';
            $query = DB::table('local_governments')->where('state_id', $id)
                ->orderby('name', 'ASC')->get(['name', 'id']);
            if ($query && $query->count() > 0):
                foreach ($query as $row) {
                    $list .= '<option value="' . $row->id . '">' . $row->name . '</option>';
                }
            endif;
            return $list;
        }
    }

    public function countries()
    {
        $query = DB::table('countries')->orderby('name', 'ASC')
            ->whereNotIn('code', [1, 44, 234])
            ->pluck('name', 'code')->all();
        return $query;
    }

    public function countries_top()
    {
        $query = DB::table('countries')->orderby('name', 'ASC')
            ->whereIn('code', [1, 44, 234])
            ->pluck('name', 'code')->all();
        return $query;
    }

    public function titles()
    {
        $json = '["ALHAJI","ALHAJA","ARCHITECT","BARRISTER","CHIEF","DEACON","DOCTOR\/DR.","ELDER","ENGINEER","EVANGELIST","EXCELLENCY","HIGHCHIEF","HONOURABLE\/HONBL","HISROYALHIGHNESS","JUSTICE","MR&MRS","M\/S","MASTER","MASTER","MAZI","MESSERS","MISS","MISTER","MISTER","MR&MRS","MR","MRS.","MISS\/MS","ALHAJI","OBA","OTUNBA","OTUNBA","PASTOR","PASTOR","PROFESSOR","REVEREND","SIR"]';

	    $query = array("ALHAJI","ALHAJA","ARCHITECT","BARRISTER","CHIEF","DEACON","DOCTOR/DR.","ELDER","ENGINEER",
		    "EVANGELIST","EXCELLENCY","HIGHCHIEF","HONOURABLE/HONBL","HISROYALHIGHNESS","JUSTICE","MR&MRS","M/S","MASTER","MASTER","MAZI","MESSERS","MISS","MISTER","MISTER","MR&MRS","MR","MRS.","MISS/MS","ALHAJI","OBA","OTUNBA","OTUNBA","PASTOR","PASTOR","PROFESSOR","REVEREND","SIR");
	    return $json;
    }

}
