<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\_Users;
use Illuminate\Http\Request;
use Input;
use Hash;

class Users extends Controller
{
    private $auth;
    private $account_id;
    private $user_id;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auth = auth()->user();
            $this->account_id = auth()->user()->account_id;
            $this->user_id = auth()->user()->id;
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $_users = new _Users();
        $select['details'] = Input::get('sDetails');
        $select['date'] = Input::get('sDate');
        $select['paginate'] = 50;
        $query = $_users->users($select, $this->account_id);

        return response()->json([
            'response' => true,
            'data' => $query,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $password = $post['password'] ? Hash::make($post['password']) : null;
        $type = $post['type'] ? $post['type'] : null;
        $email = $post['email'] ? strtolower($post['email']) : null;
        $status = isset($post['status']) ? $post['status'] : 0;

        $check = _Users::where([
            'email' => $email,
        ])->first();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'User already exist',
                ],
            ], 200);
        }

        $added = _Users::insert(array(
            'account_id' => $this->account_id,
            'email' => $email,
            'password' => $password,
            'name' => $name,
            'type' => $type,
            'status' => $status,
        ));

        if ($added) {
            return response()->json(array(
                'response' => true,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'User successfully added',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data, please retry',
                ),
            ), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $password = $post['password'] ? crypt($post['password']) : null;
        $type = $post['type'] ? $post['type'] : null;
        $email = $post['email'] ? strtolower($post['email']) : null;
        $status = isset($post['status']) ? $post['status'] : 0;

        //DB::enableQueryLog();
        $check = _Users::where([
            'email' => $email,
        ])->whereNotIn('id', [$id])->first();
        //$last_query = DB::getQueryLog();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'User already exist',
                ],
            ], 200);
        }

        $input = [];
        $input['account_id'] = $this->account_id;
        $input['email'] = $email;
        $input['name'] = $name;
        $input['type'] = $type;
        $input['status'] = $status;
        if ($password) {
            $input['password'] = $password;
        }

        $edited = _Users::where('id', $id)->update($input);

        if ($edited) {
            return response()->json(array(
                'response' => true,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'User successfully updated',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                ),
            ), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = _Users::destroy($id);

        if ($status) {
            return response()->json(array(
                'response' => true,
                'reload' => true,
                'details' => array(
                    'code' => 'DONE',
                    'message' => 'Selected value successfully deleted',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'code' => 'ERR_SAVE',
                    'message' => 'Can not delete data, please retry',
                )), 200);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $_users = new _Users();
        $select = [];
        $query = $_users->users($select, $this->account_id)->get();

        return response()->json([
            'response' => true,
            'data' => $query,
        ], 200);
    }

}
