<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\_Services;

class Reports extends Controller
{
    
    
	public function services()
	{
        $_services = new _Services();

        $now = date('Y-m-d');
        $from = date('Y-m-d', strtotime($now. ' - 31 days'));

        $daily_total = $_services->report_total($from, $now);

        //print_r($daily_total); exit;

        return response()->json([
            'response' => true,
            'data' => $daily_total,
        ], 200);

	}
	
}
