<?php

namespace App\Http\Controllers\Api;

use App\Models\_Problems;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Auth extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json([
            'response' => true,
            'data' => auth()->user()
        ], 200);
    }
}
