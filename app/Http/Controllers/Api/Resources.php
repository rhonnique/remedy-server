<?php

namespace App\Http\Controllers\Api;

use App\Models\_Resources;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Input;

class Resources extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $_data                  = new _Resources();
	    $select['details']         = Input::get( 'details' );
	    $select['date']       = Input::get( 'date' );
	    $select['get']        = true;
	    $selectCheck               = array_filter( $select );
	    $this->data['selectCheck'] = empty( $selectCheck ) ? false : true;
        $query                     = $_data->resources( $select );
        
        return response()->json([
            'response' => true,
            'data' => $query
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $phone = $post['phone'] ? $post['phone'] : null;
        $phone_alt = $post['phone_alt'] ? $post['phone_alt'] : null;
        $email = $post['email'] ? strtolower($post['email']) : null;
        //$status = isset($post['status']) ? $post['status'] : 0;

        $check = _Customers::where([
            'phone' => $phone
        ])->first();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'Customer already exist'
                ]
            ], 200);
        }

        $added = _Customers::insert(array(
            'name' => $name,
            'phone' => $phone,
            'phone_alt' => $phone_alt,
            'email' => $email
        ));
        
        if ($added) {
            return response()->json(array(
                'response' => true,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Customer successfully added',
                )
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data, please retry',
                ),
            ), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $phone = $post['phone'] ? $post['phone'] : null;
        $phone_alt = $post['phone_alt'] ? $post['phone_alt'] : null;
        $email = $post['email'] ? strtolower($post['email']) : null;

        $check = _Customers::where([
            'phone' => $phone
        ])->whereNotIn('id', [$id])->first();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'Customer already exist'
                ]
            ], 200);
        }

        $edited = _Customers::where('id', $id)->update(array(
            'name' => $name,
            'phone' => $phone,
            'phone_alt' => $phone_alt,
            'email' => $email
        ));
        
        if ($edited) {
            return response()->json(array(
                'response' => true,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Customer successfully updated',
                )
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                ),
            ), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = _Customers::destroy($id);

        if ($status) {
            return response()->json(array(
                'response' => true,
                'reload' => true,
                'details' => array(
                    'code' => 'DONE',
                    'message' => 'Selected value successfully deleted',
                )
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'code' => 'ERR_SAVE',
                    'message' => 'Can not delete data, please retry',
                )), 200);
        }
    }
}
