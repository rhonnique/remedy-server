<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\_Devices;

class Devices extends Controller
{
    private $auth;
    private $account_id;
    private $user_id;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auth = auth()->user();
            $this->account_id = auth()->user()->account_id;
            $this->user_id = auth()->user()->id;
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return $this->auth->account_id;
        
        $query = _Devices::where(['account_id'=> $this->account_id])
        ->orderby('name','asc')
        ->get();
        return response()->json([
            'response' => true,
            'data' => $query
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $desc = $post['description'] ? $post['description'] : null;
        $status = isset($post['status']) ? $post['status'] : 0;

        $check = _Devices::where([
            'name' => $name
        ])->first();

        //return response()->json($check);

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'Name already exist'
                ]
            ], 200);
        }

        $added = _Devices::insert(array(
            'account_id' => $this->account_id,
            'name' => $name,
            'description' => $desc,
            'status' => $status
        ));
        
        if ($added) {
            return response()->json(array(
                'response' => true,
                'data' => $this->index()->original,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Info successfully added',
                )
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                ),
            ), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $desc = $post['description'] ? $post['description'] : null;
        $status = isset($post['status']) ? $post['status'] : 0;

        $check = _Devices::where([
            'name' => $name
        ])->whereNotIn('id', [$id])->first();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'Name already exist'
                ]
            ], 200);
        }

        $edited = _Devices::where('id', $id)->update(array(
            'name' => $name,
            'description' => $desc,
            'status' => $status
        ));
        
        if ($edited) {
            return response()->json(array(
                'response' => true,
                'data' => $this->index()->original,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Info successfully updated',
                )
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                ),
            ), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = _Devices::destroy($id);

        if ($status) {
            return response()->json(array(
                'response' => true,
                'data' => $this->index()->original,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Selected value successfully deleted',
                )
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                )), 200);
        }
    }
}
