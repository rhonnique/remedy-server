<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\_Customers;
use App\Models\_Services;
use App\Models\_Problems;
use Illuminate\Http\Request;
use Input;
use App\Models\_Devices;

class Services extends Controller
{
    private $auth;
    private $account_id;
    private $user_id;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auth = auth()->user();
            $this->account_id = auth()->user()->account_id;
            $this->user_id = auth()->user()->id;
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $_services = new _Services();
        $select['service_id'] = Input::get('sServiceId');
        $select['details'] = Input::get('sDetails');
        $select['date'] = Input::get('sDate');
        $select['paginate'] = 50; //print_r($select); exit;
        $selectCheck = array_filter($select);
        $this->data['selectCheck'] = empty($selectCheck) ? false : true;
        $query = $_services->services($select, $this->account_id);

        return response()->json([
            'response' => true,
            'data' => $query,
            'link' => $query->links()
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        $customer_id = $post['customer_id'] ? $post['customer_id'] : null;
        $name = $post['name'] ? ucwords($post['name']) : null;
        $phone = $post['phone'] ? $post['phone'] : null;
        $problem_type = $post['problem_type'] ? $post['problem_type'] : null;
        $device_type = $post['device_type'] ? $post['device_type'] : null;
        $description = $post['description'] ? $post['description'] : null;
        $start_date = $post['start_date'] ? strtotime($post['start_date']) : null;
        $close_date = $post['close_date'] ? strtotime($post['close_date']) : null;
        $cost = $post['cost'] ? $post['cost'] : null;
        $status = $post['status'] ? $post['status'] : 0;

        $service_id = date('M') . '-' . substr(rand(), 0, 5);
        $start_date = date('Y-m-d', $start_date);
        $close_date = $close_date ? date('Y-m-d', $close_date) : null;

        $customer = _Customers::updateOrCreate(['phone' => $phone], [
            'account_id' => $this->account_id,
            'phone' => $phone,
            'name' => ucwords($name),
        ]);

        $problem = _Problems::updateOrCreate(['name' => $problem_type], [
            'account_id' => $this->account_id,
            'name' => ucwords($problem_type),
        ]);

        $device = _Devices::updateOrCreate(['name' => $device_type], [
            'account_id' => $this->account_id,
            'name' => ucwords($device_type),
        ]);

        $added = _Services::insert(array(
            'account_id' => $this->account_id,
            'service_id' => $service_id,
            'customer_id' => $customer->id,
            'problem' => $problem->name,
            'device' => $device->name,
            'description' => $description,
            'start_date' => $start_date,
            'close_date' => $close_date,
            'cost' => $cost,
            'logged_by' => $this->user_id,
            'status' => $status,
        ));

        if ($added) {
            return response()->json(array(
                'response' => true,
                'data' => $this->index()->original,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Service successfully added',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data, please retry',
                ),
            ), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $problem_type = $post['problem_type'] ? $post['problem_type'] : null;
        $device_type = $post['device_type'] ? $post['device_type'] : null;
        $description = $post['description'] ? $post['description'] : null;
        $start_date = $post['start_date'] ? strtotime($post['start_date']) : null;
        $close_date = $post['close_date'] ? strtotime($post['close_date']) : null;
        $cost = $post['cost'] ? $post['cost'] : null;
        $status = $post['status'] ? $post['status'] : 0;

        $start_date = date('Y-m-d', $start_date);
        $close_date = $close_date ? date('Y-m-d', $close_date) : null;

        $problem = _Problems::updateOrCreate(['name' => $problem_type], [
            'account_id' => $this->account_id,
            'name' => ucwords($problem_type),
        ]);

        $device = _Devices::updateOrCreate(['name' => $device_type], [
            'account_id' => $this->account_id,
            'name' => ucwords($device_type),
        ]);

        $edited = _Services::where('id', $id)->update(array(
            'problem' => $problem->name,
            'device' => $device->name,
            'description' => $description,
            'start_date' => $start_date,
            'close_date' => $close_date,
            'cost' => $cost,
            'logged_by' => $this->user_id,
            'status' => $status
        ));

        if ($edited) {
            return response()->json(array(
                'response' => true,
                'data' => $this->index()->original,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Service successfully updated'
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                ),
            ), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = _Customers::destroy($id);

        if ($status) {
            return response()->json(array(
                'response' => true,
                'reload' => true,
                'details' => array(
                    'code' => 'DONE',
                    'message' => 'Selected value successfully deleted',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'code' => 'ERR_SAVE',
                    'message' => 'Can not delete data, please retry',
                )), 200);
        }
    }
}
