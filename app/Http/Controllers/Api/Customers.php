<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\_Customers;
use Illuminate\Http\Request;
use Input;

class Customers extends Controller
{
    private $auth;
    private $account_id;
    private $user_id;

    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->auth = auth()->user();
            $this->account_id = auth()->user()->account_id;
            $this->user_id = auth()->user()->id;
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $_customers = new _Customers();
        $select['details'] = Input::get('sDetails');
        $select['date'] = Input::get('sDate');
        $select['paginate'] = 50;
        $selectCheck = array_filter($select);
        $this->data['selectCheck'] = empty($selectCheck) ? false : true;
        $query = $_customers->customers($select, $this->account_id);

        return response()->json([
            'response' => true,
            'data' => $query,
        ], 200);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo 777;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $phone = $post['phone'] ? $post['phone'] : null;
        $phone_alt = $post['phone_alt'] ? $post['phone_alt'] : null;
        $email = $post['email'] ? strtolower($post['email']) : null;
        //$status = isset($post['status']) ? $post['status'] : 0;

        $check = _Customers::where([
            'phone' => $phone,
        ])->first();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'Customer already exist',
                ],
            ], 200);
        }

        $added = _Customers::insert(array(
            'account_id' => $this->account_id,
            'name' => $name,
            'phone' => $phone,
            'phone_alt' => $phone_alt,
            'email' => $email,
        ));

        if ($added) {
            return response()->json(array(
                'response' => true,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Customer successfully added',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data, please retry',
                ),
            ), 200);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = $request->all();
        $name = $post['name'] ? ucwords($post['name']) : null;
        $phone = $post['phone'] ? $post['phone'] : null;
        $phone_alt = $post['phone_alt'] ? $post['phone_alt'] : null;
        $email = $post['email'] ? strtolower($post['email']) : null;

        //DB::enableQueryLog();
        $check = _Customers::where([
            'phone' => $phone,
        ])->whereNotIn('id', [$id])->first();
        //$last_query = DB::getQueryLog();

        if ($check) {
            return response()->json([
                'response' => false,
                'details' => [
                    'status_code' => 'ERR_EXIST',
                    'status_msg' => 'Customer already exist',
                ],
            ], 200);
        }

        $edited = _Customers::where('id', $id)->update(array(
            'name' => $name,
            'phone' => $phone,
            'phone_alt' => $phone_alt,
            'email' => $email,
        ));

        if ($edited) {
            return response()->json(array(
                'response' => true,
                'details' => array(
                    'status_code' => 'DONE',
                    'status_msg' => 'Customer successfully updated',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'status_code' => 'ERR_SAVE',
                    'status_msg' => 'Can not save data',
                ),
            ), 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = _Customers::destroy($id);

        if ($status) {
            return response()->json(array(
                'response' => true,
                'reload' => true,
                'details' => array(
                    'code' => 'DONE',
                    'message' => 'Selected value successfully deleted',
                ),
            ), 200);
        } else {
            return response()->json(array(
                'response' => false,
                'details' => array(
                    'code' => 'ERR_SAVE',
                    'message' => 'Can not delete data, please retry',
                )), 200);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function all()
    {
        $_customers = new _Customers();
        $select = [];
        $query = $_customers->customers($select, $this->account_id)->get();

        return response()->json([
            'response' => true,
            'data' => $query,
        ], 200);
    }

    
}
