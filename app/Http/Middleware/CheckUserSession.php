<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\_account;
use Request;


class CheckUserSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	public function handle($request, Closure $next)
	{
		if (!$request->session()->has('user_id')) {
			if(Request::ajax())
			{
				return response()->json( [
					"Response" => false,
					"Details"  => [
						"Code" => "ERR_AUTH",
						"Msg" => "Authentication failed"
					]
				], 401 );
			}

			return redirect('login');
		}

		$user_id = $request->session()->get('user_id');

		$_account = new _account();
		$account   = $_account->details( $user_id );

		if (!$account) {
			$request->session()->forget('user_id');

			if(Request::ajax())
			{
				return response()->json( [
					"Response" => false,
					"Details"  => [
						"Code" => "ERR_AUTH",
						"Msg" => "Authentication failed"
					]
				], 401 );
			}

			return redirect('login');
		}

		//$request->attributes->set('user', $account->name);
		$request->request->add(['user_data' => [
			'user_id' => $account->user_id,
			'email' => $account->email,
			'name' => $account->name,
			'phone' => $account->phone,
			'status' => $account->status,
			'balance' => $account->balance,
			'country' => $account->country,
		]]);

		return $next($request);
	}
}
