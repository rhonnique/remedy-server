<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\_Admin;
use Request;


class CheckAdminSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	public function handle($request, Closure $next)
	{  //echo 144; exit;
		if (!$request->session()->has('admin_5984934')) {
			if(Request::ajax())
			{
				return response()->json( [
					"Response" => false,
					"Details"  => [
						"Code" => "ERR_AUTH",
						"Msg" => "Authentication failed"
					]
				], 401 );
			}
			return redirect('admin/login');
		}

		$id = $request->session()->get('admin_5984934')['admin_id'];
		$account   = _Admin::find($id);

		//print_r($account); exit;

		if (!$account) {
			$request->session()->forget('admin_5984934');

			if(Request::ajax())
			{
				return response()->json( [
					"Response" => false,
					"Details"  => [
						"Code" => "ERR_AUTH",
						"Msg" => "Authentication failed"
					]
				], 401 );
			}

			return redirect('admin/login');
		}

		//$request->attributes->set('user', $account->name);
		$request->request->add(['admin_data' => [
			'admin_id' => $account->admin_id,
			'username' => $account->username,
			'name' => $account->name,
			'status' => $account->status
		]]);

		return $next($request);
	}
}
