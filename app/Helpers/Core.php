<?php


function resetEdit( $url ) {
	$page   = Input::get( 'page' );
	$append = $page ? "page=" . $page : "";
	$url    = $url . "?" . $append;

	//return $url = substr($url,-1);

	return $url = substr( $url, - 1 ) == "?" ? substr( $url, 0, - 1 ) : $url;


	//return $url."?".$append;
}


function getEditLink( $url, $id ) {
	$urlString = Request::getQueryString();

	return url( $url . "?mode=edit&id=" . $id . "&" . $urlString );

}

function resetEditLink() {
	return substr( current_url(), 0, - strlen( substr( strrchr( current_url(), "edit" ), 0, 20 ) ) );

    /*
	Request::fullUrl();
	// Returns: http://laravel.dev/test?test=1
	Request::url();
	// Returns: http://laravel.dev/test
	Request::path();
	// Returns: test
	Request::root();
	// Returns: http://laravel.dev
    */
}

function format_status($status, $msg = null){
	switch($status){
		case 0:
		$msg = $msg?$msg:'Inactive';
		$s = '<span class="badge badge-outline badge-default">'.$msg.'</span>';
		break;
		case 1:
		$msg = $msg?$msg:'Active';
		$s = '<span class="badge badge-outline badge-success">'.$msg.'</span>';
		break;
		case -1:
		$msg = $msg?$msg:'Error';
		$s = '<span class="badge badge-outline badge-danger">'.$msg.'</span>';
		break;
		default:
		$msg = $msg?$msg:'None';
		$s = '<span class="badge badge-outline badge-default">'.$msg.'</span>';
	}
	return $s;
}


function format_search_date($date){
	$date = urldecode($date);
	$date = explode('-',$date);
	$date0 = trim($date[0]);
	$date1 = trim($date[1]);
	$date0 = str_replace('.','-',$date0);
	$date1 = str_replace('.','-',$date1);
	$date0 = strtotime($date0);
	$date1 = strtotime($date1);
	$date0 = date('Y-m-d',$date0);
	$date1 = date('Y-m-d',$date1);
	return array($date0, $date1);
}