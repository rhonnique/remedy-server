<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;


Route::middleware('jwt.auth')->get('auth', function (Request $request) {
    return auth()->user();
});

Route::get('auth/logout', function (Request $request) {
    \JWTAuth::parseToken()->invalidate();
    return response([
        'status' => 'success',
        'msg' => 'Logged out Successfully.',
    ], 200);
});

Route::post('login', 'Api\Login@loginSubmit');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('auth', 'Api\Auth');
    Route::resource('dashboard', 'Api\Dashboard');

    Route::resource('problems', 'Api\Problems');
    Route::post('problems/store', 'Api\Problems@store');
    Route::post('problems/update/{id}', 'Api\Problems@update');
    Route::get('problems/destroy/{id}', 'Api\Problems@destroy');

    Route::resource('devices', 'Api\Devices');
    Route::post('devices/store', 'Api\Devices@store');
    Route::post('devices/update/{id}', 'Api\Devices@update');
    Route::get('devices/destroy/{id}', 'Api\Devices@destroy');

    Route::get('customers', 'Api\Customers@index');
    Route::get('customers/all', 'Api\Customers@all');
    Route::post('customers/store', 'Api\Customers@store');
    Route::post('customers/update/{id}', 'Api\Customers@update');
    Route::get('customers/destroy/{id}', 'Api\Customers@destroy');

    Route::resource('services', 'Api\Services');
    Route::post('services/store', 'Api\Services@store');
    Route::post('services/update/{id}', 'Api\Services@update');
    Route::get('services/destroy/{id}', 'Api\Services@destroy');

    Route::resource('resources', 'Api\Resources');
    Route::post('resources/store', 'Api\Resources@store');
    Route::post('resources/update/{id}', 'Api\Resources@update');
    Route::get('resources/destroy/{id}', 'Api\Resources@destroy');

    Route::resource('users', 'Api\Users');
    Route::post('users/store', 'Api\Users@store');
    Route::post('users/update/{id}', 'Api\Users@update');
    Route::get('users/destroy/{id}', 'Api\Users@destroy');

    Route::get('reports/services', 'Api\Reports@services');

});































 Route::get('faker33', function () {
    $faker = \Faker\Factory::create();

    $problems = DB::table('problems')->pluck('id','name')->all();
    print_r($problems); exit;

/*
    echo $faker->randomFloat($nbMaxDecimals = NULL, $min = 3000, $max = 45000); exit;

    $customers = DB::table('customers')->pluck('id')->all();
    for ($i=0; $i < 100; $i++) {
    echo $customers[array_rand($customers, 3)[0]].'<br/>'; 
    }
    exit;

    $input = array("Neo", "Morpheus", "Trinity", "Cypher", "Tank");
    echo $input[array_rand($input, 3)[0]]; exit;

$rand_keys = array_rand($input, 3);
echo $input[$rand_keys[0]] . "\n";
exit;


    for ($i=0; $i < 5; $i++) {
        //echo $faker->numberBetween($min = 100000, $max = 900000).'<br/>';
        echo $faker->randomElement( ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']);
    }

    exit;
*/
   


    $null = [];
    $close = [];
    $dateStart = [];
    $dateCloses = [];
    $months = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];
    //$phones = ['iPhone 7','IPhone 6 Plus','IPhone 3GS','IPhone X','IPhone 5c','IPhone 8 Plus','IPhone 4S','AUG','SEP','OCT','NOV','DEC'];
    //$devices = DB::table('devices')->pluck('name')->all();
    //$devices = DB::table('devices')->where('(substring(device, 1, 6)','iphone')->pluck('name')->all();
    $devices = DB::table('devices')->where(DB::raw('substr(name, 1, 6)'), '=' , 'iphone')->pluck('name')->all();
    //print_r($devices); exit;
    $customers = DB::table('customers')->pluck('id')->all();
    $problems = DB::table('problems')->pluck('name')->all();
    $admin = DB::table('problems')->pluck('name')->all();
    //$users = DB::table('users')->pluck('id')->all();

/*
    $query = collect( DB::select( "SELECT * FROM ngmobile_ng.jobs where (substring(device, 1, 5) = 'apple' )" ) );
    foreach($query as $row){
        DB::table('jobs')->where(['id' => $row->id])
		           ->update([
			           'device' => $devices[array_rand($devices, 3)[0]],
			           'problem' => $problems[array_rand($problems, 3)[0]],
		           ] );
    }
    exit;
    */









    for ($i=0; $i < 50; $i++) {
        $null[] = null;
    }
    for ($i=0; $i < 300; $i++) {
        //$date = date('Y-m-d H:i:s', $date);
        //$date = $faker->dateTime($startDate = '+2 years');
        $date = $faker->dateTimeInInterval($startDate = '-2 years', $endDate = '+2 years', $timezone = null)->getTimestamp();
        $date = date('Y-m-d', $date);
        $dateStart[] = $date;
        $dateClose = $faker->dateTimeInInterval($startDate = '-1 years', $endDate = '+1 years', $timezone = null)->getTimestamp();
        $dateClose = date('Y-m-d', $dateClose);
        $dateCloses[] = $dateClose;
    }

    $close = array_merge($null, $dateCloses);

    for ($i=0; $i < 400; $i++) {

        $sdate = $dateStart[array_rand($dateStart, 3)[0]];
        $close_date = $close[array_rand($close, 3)[0]];

        $insert = [];
        $insert['account_id'] = 1;
        $insert['job_id'] = $faker->randomElement($months).'-'.$faker->numberBetween($min = 100000, $max = 900000);
        $insert['customer_id'] = $customers[array_rand($customers, 3)[0]];
        $insert['problem'] = $problems[array_rand($problems, 3)[0]];
        $insert['device'] = $devices[array_rand($devices, 3)[0]];
        $insert['cost'] = $faker->randomFloat($nbMaxDecimals = NULL, $min = 3000, $max = 45000);
        $insert['start_date'] = $sdate;
        if($close_date != null){
            $extra = $faker->randomElement($array = array ('1','2','3','4','5','7','3','2','3','1'));
            $insert['close_date'] = date('Y-m-d', strtotime($sdate. ' + '.$extra.' days'));
        }
        $insert['created_at'] = $sdate;
        $insert['updated_at'] = $sdate;
        $insert['logged_by'] = $faker->randomElement($array = array ('1','2'));

        DB::table( 'jobs' )->insert($insert);

    }
 });




        

Route::get('jsonss', function () {
    
    $jsonString = file_get_contents(base_path('mac.json'));
    $data = (array) json_decode($jsonString, true);
    //print_r($data[0]['name']); exit;
    $phones = [];

    foreach ($data as $key => $value){
        $phones[] = $value['model'];
    }

    $phones = array_unique($phones);

    //print_r($phones); exit;

    /**/
    foreach ($phones as $value){
        DB::table( 'devices' )->insert([
            'account_id' => 1,
            'name' => $value
        ]);
    }
    
});










