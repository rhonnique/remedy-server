<?php

/*
use Illuminate\Http\Request;
//use JWTAuth;


|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|


Route::middleware('jwt.auth')->get('auth', function (Request $request) {
    return auth()->user();
});

Route::get('auth/logout', function (Request $request) {
    \JWTAuth::parseToken()->invalidate();
    return response([
        'status' => 'success',
        'msg' => 'Logged out Successfully.',
    ], 200);
});

Route::post('login', 'Api\Login@loginSubmit');
Route::group(['middleware' => 'jwt.auth'], function () {
    Route::resource('auth', 'Api\Auth');
    Route::resource('dashboard', 'Api\Dashboard');

    Route::resource('problems', 'Api\Problems');
    Route::post('problems/store', 'Api\Problems@store');
    Route::post('problems/update/{id}', 'Api\Problems@update');
    Route::get('problems/destroy/{id}', 'Api\Problems@destroy');

    Route::resource('devices', 'Api\Devices');
    Route::post('devices/store', 'Api\Devices@store');
    Route::post('devices/update/{id}', 'Api\Devices@update');
    Route::get('devices/destroy/{id}', 'Api\Devices@destroy');

    Route::get('customers', 'Api\Customers@index');
    Route::get('customers/all', 'Api\Customers@all');
    Route::post('customers/store', 'Api\Customers@store');
    Route::post('customers/update/{id}', 'Api\Customers@update');
    Route::get('customers/destroy/{id}', 'Api\Customers@destroy');

    Route::resource('services', 'Api\Services');
    Route::post('services/store', 'Api\Services@store');
    Route::post('services/update/{id}', 'Api\Services@update');
    Route::get('services/destroy/{id}', 'Api\Services@destroy');

    Route::resource('resources', 'Api\Resources');
    Route::post('resources/store', 'Api\Resources@store');
    Route::post('resources/update/{id}', 'Api\Resources@update');
    Route::get('resources/destroy/{id}', 'Api\Resources@destroy');

    Route::resource('users', 'Api\Users');
    Route::post('users/store', 'Api\Users@store');
    Route::post('users/update/{id}', 'Api\Users@update');
    Route::get('users/destroy/{id}', 'Api\Users@destroy');

    Route::get('reports/services', 'Api\Reports@services');

});

 */