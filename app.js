

$(function() {
    $( ".pagination a" ).click(function( event ) {
        event.preventDefault();
        //alert($(this).attr("href"));

        $( ".page-list-view" ).load($(this).attr("href"));

        /*
        if($(this).attr("href")) {
            var next = $(this).attr("href");
            $( ".page-list-view" ).load(site_url + 'contact-list/contact-list' + next);
        } else {
            $( ".page-list-view" ).load(site_url + 'contact-list/contact-list');
        }
        */
    });


});


function __auth(){
    window.location = site_url + 'logout';
    /*
    if(data.trim() == 411) {
        return window.location = site_url + 'logout';
    } else {
        return true;
    }
    */
}


function blockUI(){
    $.blockUI({
        message: $('.ui-block'),
        css: {
            border: 'none',
            backgroundColor: 'none',
            'border-radius': '5px'
        }
    });
}


function unblockUI(){
    $.unblockUI();
}


function blocked(classes){
    $("."+classes).append('<div class="blocker"></div>');
    //$("#btnChangeOrder").attr("disabled", true);
}

function unBlock(classes){
    $("."+classes+" .blocker").remove();
    //$("#btnChangeOrder").attr("disabled", false);
}


function inlineBlocked(classes){
    $("."+classes).prepend('<div class="inline-blocker"></div>');
}

function inlineUnblock(classes){
    $("."+classes+" .inline-blocker").remove();
}


function cellBlock(id,msg,cls){
    $(".tr-"+id+" .cell-action .process").remove();
    var close = '';
    if(cls == 'bg-primary' || cls == 'bg-success' || cls == 'bg-danger'){
        close = '<button type="button" class="close" data-dismiss="alert">×</button>';
    }
    $(".tr-"+id+" .cell-action").prepend('<div class="process '+cls+'">'+msg+close+'</div>');
}


function defaultModal(data) {
    var size = data.size ? data.size : "";
    var title = data.title ? data.title : "&nbsp;";
    //$(".modal-dialog").addClass(size);
    $(".modal-title").html(title);

    $('#default-modal').modal({
        show: true,
        keyboard: false,
        backdrop: 'static'
    });

    $(".modal-dialog").addClass(size);
    $(".modal-dialog .modal-body").removeClass('no-pad');

    //$(".modal-body").removeClass('no-pad');
    //alert(url);

    if (data.path) {
        var url = site_url + data.path;
        console.log(url);
        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                $(".modal-body").html('<span class="font-600 text-warning p-20' +
                'display-block">loading...</span>');
            },
            success: function (data) {
                if(data == 411){ __auth(); return false; }
                $(".modal-body").html(data);
            },
            error: function () {
                $(".modal-body").html('<span class="font-600 text-danger p-20' +
                'display-block">Page Not Loaded</span>');
            }
        });
    }


    if (data.noPad) {
        $(".modal-body").addClass("no-pad");
    }
}


function closeModal(){
    $('#default-modal').modal('hide');
    //$("#default-modal").removeClass('modal-lg').removeClass('modal-sm');
}


function pageData(data){
    var attach = data.attached;
    var path = data.path;


    var url = site_url + path; //alert(attach);
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function () { //alert(1);
            blocked("page-data"); //return false;
        },
        success: function (data) {
            if(data == 411){ __auth(); return false; }
            $("."+attach).html(data);


            $('.'+attach+' table').dataTable({
                dom: '<"datatable-header"Tfl>t<"datatable-footer"ip>',
                "pageLength": 500,
                tableTools: {
                    sRowSelect: "single",
                    sSwfPath: ajaxApp + "media/swf/copy_csv_xls_pdf.swf",
                    aButtons: [
                        {
                            sExtends:    'copy',
                            sButtonText: 'Copy',
                            sButtonClass: 'btn btn-default'
                        },
                        {
                            sExtends:    'print',
                            sButtonText: 'Print',
                            sButtonClass: 'btn btn-default'
                        },
                        {
                            sExtends:    'collection',
                            sButtonText: 'Save <span class="caret"></span>',
                            sButtonClass: 'btn btn-primary',
                            aButtons:    [ 'csv', 'xls', 'pdf' ]
                        }
                    ]
                }
            });

            $('.dataTables_filter input[type=search]').attr('placeholder','Type to filter...');

            $(".dataTables_length select").select2({
                minimumResultsForSearch: "-1"
            });

            unBlock("page-data");


        },
        error: function () {
            unBlock("page-data");
            alert('Cannot load requested page!');
        }
    });


}


function createAccount() {

    var firstname = $("#firstname").val();
    var othernames = $("#othernames").val();
    var phone = $("#phone").val();
    var gender = $("#gender").val();
    var maritalstatus = $("#maritalstatus").val();
    var nationality = $("#nationality").val();
    var residence = $("#residence").val();
    var homeaddress = $("#homeaddress").val();
    var hometown = $("#hometown").val();
    var religion = $("[name=religion]").val();
    var churchaddress = $("#churchaddress").val();
    var churchname = $("#churchname").val();
    var prevattendance = $("[name=prevattendance]").val();
    var secquestion = $("#secquestion").val();
    var secanswer = $("#secanswer").val();
    var password = $("#password").val();
    var password2 = $("#password2").val();
    var email = $("#email").val();
    var prefattendance = $("[name=prefattendance]:checked").val();



    if(!firstname){
        alert('First name is required!');
        $("#firstname").focus();
        return false;
    }

    if(!othernames){
        alert('Other name is required!');
        $("#othernames").focus();
        return false;
    }

    if(!email){
        alert('Email is required!');
        $("#email").focus();
        return false;
    }

    if(!phone){
        alert('Phone number is required!');
        $("#phone").focus();
        return false;
    }

    if(!gender){
        alert('Gender is required!');
        $("#gender").focus();
        return false;
    }


    if(!password) {
        alert('Password is required!');
        $("#password").focus();
        return false;
    }

    if(!password2){
        alert('Confirm password is required!');
        $("#secquestion").focus();
        return false;
    }


    if(password != password2){
        alert('Password does not match!');
        $("#password2").focus();
        return false;
    }


    if(!secquestion){
        alert('Security question is required!');
        $("#secquestion").focus();
        return false;
    }

    if(!secanswer){
        alert('Security answer is required!');
        $("#secanswer").focus();
        return false;
    }




    var data = {
        'btnRegister': true,
        'firstname': firstname,
        'othernames': othernames,
        'phone': phone,
        'gender': gender,
        'maritalstatus': maritalstatus,
        'nationality': nationality,
        'residence': residence,
        'homeaddress': homeaddress,
        'hometown': hometown,
        'religion': religion,
        'churchaddress': churchaddress,
        'churchname': churchname,
        'prevattendance': prevattendance,
        'password': password,
        'password2': password2,
        'secquestion': secquestion,
        'secanswer': secanswer,
        'email': email,
        'prefattendance': prefattendance
    }

    var url = site_url + 'participant/register';

    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if(data == 411){ __auth(); return false; }
            unblockUI();
            if (data.status == 1) {

                defaultModal({
                    "path": 'participant/details/'+data.unique_id,
                    "title": "Participant Details",
                    "size": "modal-lg"
                });

            } else if (data.status == 2) {
                alert("Invalid data, please try again!");
            } else if (data.status == 3) {
                alert("Email Address already exist, please try again!");
            } else {
                alert('An error occurred, please re-try!');
            }
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}




function editAccount(id, mode) {

    var firstname = $("#firstname").val();
    var othernames = $("#othernames").val();
    var phone = $("#phone").val();
    var gender = $("#gender").val();
    var maritalstatus = $("#maritalstatus").val();
    var nationality = $("#nationality").val();
    var residence = $("#residence").val();
    var homeaddress = $("#homeaddress").val();
    var hometown = $("#hometown").val();
    var religion = $("[name=religion]").val();
    var churchaddress = $("#churchaddress").val();
    var churchname = $("#churchname").val();
    var prevattendance = $("[name=prevattendance]").val();
    var secquestion = $("#secquestion").val();
    var secanswer = $("#secanswer").val();
    var password = $("#password").val();
    var password2 = $("#password2").val();
    var email = $("#email").val();
    var prefattendance = $("[name=prefattendance]:checked").val();
    mode = mode?mode:1;



    if(!firstname){
        alert('First name is required!');
        $("#firstname").focus();
        return false;
    }

    if(!othernames){
        alert('Other name is required!');
        $("#othernames").focus();
        return false;
    }

    if(!email){
        alert('Email is required!');
        $("#email").focus();
        return false;
    }

    if(!phone){
        alert('Phone number is required!');
        $("#phone").focus();
        return false;
    }

    if(!gender){
        alert('Gender is required!');
        $("#gender").focus();
        return false;
    }



    var data = {
        'btnEdit': true,
        'id': id,
        'firstname': firstname,
        'othernames': othernames,
        'phone': phone,
        'gender': gender,
        'maritalstatus': maritalstatus,
        'nationality': nationality,
        'residence': residence,
        'homeaddress': homeaddress,
        'hometown': hometown,
        'religion': religion,
        'churchaddress': churchaddress,
        'churchname': churchname,
        'prevattendance': prevattendance,
        'password': password,
        'password2': password2,
        'secquestion': secquestion,
        'secanswer': secanswer,
        'email': email,
        'prefattendance': prefattendance,
        'mode': mode
    }

    var url = site_url + 'participant/edit';

    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) { //alert(data.status); return false;
            if(data == 411){ __auth(); return false; }
            unblockUI();
            if (data.status == 1) {

                defaultModal({
                    "path": 'participant/details/'+data.unique_id,
                    "title": "Participant Details",
                    "size": "modal-lg"
                });

            } else if (data.status == 2) {
                alert("Invalid data, please try again!");
            } else if (data.status == 3) {
                alert("Email Address already exist, please try again!");
            } else {
                alert('An error occurred, please re-try!');
            }
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function showContactDetails(id){
    console.log('participant/details/'+id);
    defaultModal({
        "path": 'participant/details/'+id,
        "title": "Participant Details",
        "size": "modal-lg"
    });
}


function showRegister(){
    console.log('participant/register/');
    defaultModal({
        "path": 'participant/register/',
        "title": "Register Participant",
        "size": "modal-lg"
    });
}





function showSearch(){
    console.log('search/');
    defaultModal({
        "path": 'search',
        "title": "Search Participant",
        "size": "modal-lg"
    });
}


function showAssignHotelId(assignedId){
    console.log('assigned-rooms/assign-key/'+assignedId);
    defaultModal({
        "path": 'assigned-rooms/assign-key/'+assignedId,
        "title": "Assign Room Key Id"
    });
}



function loadPopAssignKey(room_number){
    popover("ASSIGN KEY");
    var url = site_url + "assigned-rooms/assign-key/"+room_number+'/1';
    console.log(url);

    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function () {
            $(".inline-popover-body").html('loading payment, please wait...');
        },
        success: function (data) {
            if(data == 411){ __auth(); return false; }
            $(".inline-popover-body").html(data);
        },
        error: function () {
            alert('error');
        }
    });
}


function popover(title){
    $(".modal-body").append('<div class="inline-popover-overlay">' +
    '<div class="inline-popover">' +
    '<div class="inline-popover-header">'+title+'</div>' +
    '<button type="button" class="close-inline-popover" onclick="closePopOver()">×</button>' +
    '<div class="inline-popover-body"></div>' +
    '</div>' +
    '</div>');
}



function addPaymentLoad(unique_id){

    popover("ADD PAYMENT");

    var url = site_url + "booking/payment/"+unique_id;
    console.log(url);

    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function () {
            $(".inline-popover-body").html('loading payment, please wait...');
        },
        success: function (data) {
            if(data == 411){ __auth(); return false; }
            $(".inline-popover-body").html(data);
        },
        error: function () {
            alert('error');
        }
    });
}


function closePopOver(){
    $(".inline-popover-overlay").remove();
}


function savePayment(unique_id){
    var payment_mode = $("[name=payment_mode]").val();
    var trans_id = $("[name=trans_id]").val();
    var comment = $("[name=comment]").val();
    var amount_paid = $("[name=amount_paid]").val();

    if (!payment_mode) {
        alert("Please select mode of payment!");
        $("[name=payment_mode]").focus();
        return false;
    }

    if (!amount_paid) {
        alert("Please add amount paid!");
        $("[name=amount_paid]").focus();
        return false;
    }

    if (isNaN(amount_paid)) {
        alert("Amount paid is invalid!");
        $("[name=amount_paid]").focus();
        return false;
    }

    var data = {
        'btnAddPaymentSubmit': true,
        'payment_mode': payment_mode,
        'trans_id': trans_id,
        'comment': comment,
        'amount_paid': amount_paid,
        'payment_mode': payment_mode,
        'unique_id': unique_id
    }

    var url = site_url + 'booking/payment/';
    console.log(url);

    $.ajaxSetup ({ cache: false });
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function() {
            inlineBlocked("inline-popover");
        },
        success: function(data) { //alert(data.status); return false;
            if(data == 411){ __auth(); return false; }
            if(data.status == 1) {
                $( "ul.list-selected-booking li.unique-id-"+unique_id )
                    .load( site_url + 'booking/details?details='+unique_id );
                closePopOver();
            } else if(data.status == 2){
                alert("Payment with this transaction id has been logged!");
            } else {
                alert("An error occurred, please re-try!");
            }

            inlineUnblock("inline-popover");
        },
        error: function() {
            inlineUnblock("inline-popover");
            alert("An error occurred, please re-try!");
        }
    });
}


function reSendLoginId(id){

    var data = {
        'btnResendId': true,
        'id': id
    }

    var url = site_url + 'participant/resend-login/'+id;
    console.log(url);

    $.ajaxSetup ({ cache: false });
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function() {
            //blockUI()
            //cellBlock(id,'requesting login id, please wait...');
            //return false;
        },
        success: function(data) { alert(data.status+' '+data.response);
            if(data == 411){ __auth(); return false; }
            if(data.status == 1) {
                cellBlock(id,'Login ID successfully generated!','bg-success');
            } else if(data.status == 2){
                cellBlock(id,'Invalid request, please re-try!','bg-danger');
            } else if(data.status == 3){
                cellBlock(id,'Invalid request, please re-try!','bg-danger');
            } else if(data.status == 0){
                cellBlock(id,'Error response: '+data.response,'bg-danger');
            } else {
                cellBlock(id,'Invalid request, please re-try!','bg-danger');
            }

            $(".tr-"+id+" .cell-3").html(data.login_id);
        },
        error: function() {
            cellBlock(id,'Invalid request, please re-try!','bg-danger');
        }
    });
}



function getSearch(value) {
    if (value == '') {
        $(".search-status, ul.booking-added").html("");
    } else {

        var url = site_url + "search?p="
            + encodeURIComponent(value);
        console.log(url);

        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                $(".search-status").html('searching, please wait...')
                    .addClass('text-info text-bold').show();
            },
            success: function (data) {
                if(data == 411){ __auth(); return false; }
                $("ul.booking-added").html(data);
                $(".search-status").html('');
            },
            error: function () {
                alert('error');
            }
        });
    }
}


function showBookRoom(unique_id, manage){
    manage = manage && unique_id != 0?manage:0;
    defaultModal({
        "path": 'booking/'+unique_id+'?manage='+manage,
        "title": "Booking Desk",
        "size": "modal-lg"
    });
}


function showReAssignRoom(){
    console.log('participant/book-room/1');
    defaultModal({
        "path": 'participant/book-room/1',
        "title": "Reassign Room",
        "size": "modal-lg"
    });
}


function showRoomDetails(id){
    //console.log('assigned-rooms/details/'+id);
    defaultModal({
        "path": 'assigned-rooms/details/'+id+'/'+event_id,
        "title": "Assigned Room Details",
        "size": "modal-lg"
    });
}


function showEditInfo(id, mode){
    console.log('participant/edit/'+id+'/'+mode);
    defaultModal({
        "path": 'participant/edit/'+id+'/'+mode,
        "title": "Edit Participant Details",
        "size": "modal-lg"
    });
}


/*
function getBookingSearch(url, value){
    if (value == '' || value.length < 3) {
        $("ul.search-results").html("");
        checkAddBooking();
    } else {

        var url = site_url + "participant/book-room/"+url+"?p="
            + encodeURIComponent(value);
        console.log(url);

        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                $("ul.search-results").html("searching, please wait...");
            },
            success: function (data) {
                $("ul.search-results").html(data);
            },
            error: function () {
                alert('error');
            }
        });
    }
}
*/




function getBookingSearch(value){
    if (value == '' || value.length < 2) {
        $("ul.search-results").html("");
        checkAddBooking();
    } else {
        var url = site_url + "booking?search=" + encodeURIComponent(value);
        console.log(url);

        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                $("ul.search-results").html("searching, please wait...");
            },
            success: function (data) {
                if(data == 411){ __auth(); return false; }
                $("ul.search-results").html(data);
            },
            error: function () {
                alert('error');
            }
        });
    }
}




function loadPartner(unique_id, room_number){
    var url = site_url + "participant/partner/"+unique_id+"/"+room_number;
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function () {
            $(".assigned-"+unique_id+" span").html("checking..");
        },
        success: function (data) {
            if(data == 411){ __auth(); return false; }
            alert(data.trim());
            $(".assigned-"+unique_id+" span").html("view partner(s)");
        },
        error: function () {
            alert("An error occurred, please re-try!");
            $(".assigned-"+unique_id+" span").html("view partner(s)");
        }
    });
}



function addToBooking(id, manage){
    var addedBooking = new Array();
    $.each($('.booking-checked'), function(index) {
        addedBooking[index] = $(this).val();
    });

    if(jQuery.inArray(String(id), addedBooking ) < 0 && addedBooking.length <= 2){
    //if ($.inArray(id, addedBooking) < 0){
        manage = manage?manage:0;
        var url = site_url + "booking/details?details="+id+'&manage='+manage;
        console.log(url);
        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                $("ul.booking-added").append('<li class="unique-id-'+id+'"></li>');
                $("ul.booking-added li:last").html("adding, please wait...");
            },
            success: function (data) {
                if(data == 411){ __auth(); return false; }
                $("ul.booking-added li:last")
                    .html(data);
                checkAddBooking();
                resetResv();
            },
            error: function () {
                $("ul.booking-added li:last")
                    .html('Can not add participant!');
            }
        });
    }

}


function resetResv(){
    $("#room_type").val("");
    $(".pref-list").hide();
    $("[name=pref_type]").prop("checked", false);
}



function checkAddBooking(){

    /**/
    var part = $("[name=check_booking]");

    if(part.length > 0){
        $(".booking-actions").show();
    } else {
        $(".booking-actions").hide();
    }


    /*
     var services = [];

     $('.selectedService:checked').each(function() {
     services.push($(this).val());
     });

     alert(services);
     */

}


function removeP(id){
    $("ul.list-selected-booking li.unique-id-"+id).remove();
    resetResv();
}





function setRoomType(value){

    var part = $("ul.list-selected-booking [name=check_booking]");

    if (value == 1) {
        $(".pref-list").show();
        $(".pref-pp").show();
        $(".pref-np").hide();

        $(".friend-share-type, .friend-id").hide();
        $("[name=friend_share_type]:first, [name=pref_type]:first").prop('checked', true);
        $("#pp").prop('checked', false);
        $("#friend_id").prop('disabled', true);
    } else if(value == 2 || value == 3){
        $(".pref-list").show();
        $(".pref-pp").hide();
        $(".pref-np").show();
    } else {
        $(".pref-list").hide();
        $(".pref-pp").hide();
        $(".pref-np").hide();
    }

}


/*
function setPrefType(value) {
    if (value == "P" || value == "PP") {
        $(".friend-share-type").show();
    } else {
        $(".friend-share-type, .friend-id").hide();
        $("[name=friend_share_type]:first").prop('checked', true);
        $("#friend_id").prop('disabled', true);
    }
}


function setShareType(value) {
    if (value == 2) {
        $(".friend-id").show();
        $("#friend_id").prop('disabled', false);
    } else {
        $(".friend-id").hide();
        $("#friend_id").prop('disabled', true);
    }

}
*/

function setPartner(){
    var part = $("ul.list-selected-booking [name=check_booking]");
    var pp_select = $("ul.list-selected-booking [name=pp_select]");

    if($("#PP").is(":checked")) {
        if (pp_select.length == 1) {
            $(".resv-data").hide();
        } else {
            $(".resv-data").show();
        }
    } else {
        $(".resv-data").show();
    }


    /*
    if($("#PP").is(":checked")){
        $(".partner-id").show();
        $("#partner_id").prop('disabled', false);
    } else {
        $(".partner-id").hide();
        $("#partner_id").prop('disabled', true);
    }
    */
}






function prefSubmit() {
    var room_type = $("[name=room_type]").val();
    var pref_type = $("[name=pref_type]:checked").val();
    var participation = $("#participation").val();

    var part = $("ul.list-selected-booking [name=check_booking]");
    var pp_select = $("ul.list-selected-booking [name=pp_select]");
    var selected_paid = $("ul.list-selected-booking [name=selected_paid]");

    var participant = [];
    $('.booking-checked').each(function() {
        participant.push($(this).val());
    });

    if (!room_type) {
        alert("Please select room type!");
        $("[name=room_type]").focus();
        return false;
    }

    /*
    if(!pref_type){
        alert("no pref_type!");
    }

    alert(room_type+' '+pref_type+' '+part.length);
*/
    if(room_type == 1 && !pref_type && part.length > 1){
        alert("You can only select one participant for this option!");
        return false;
    }

    //var pref_type = '';
    var friend_share_type = '';
    var friend_id = '';
    //var partner_id = '';


    if($("#PP").is(":checked")){
        if(participant.length != 2){
            alert("Please select partner!");
            return false;
        }

        pref_type = "PP";
    } else if (pref_type == "N" || pref_type == "P") {
        friend_share_type = $("[name=friend_share_type]:checked").val();
        if (friend_share_type == 2) {
            friend_id = $("#friend_id").val();

            if(!friend_id){
                alert('Participant ID is required!');
                $("#friend_id").focus();
                return false;
            }
        }

        if (pref_type == "N"){
            friend_share_type = '';
        }

    } else {
        pref_type = '';
    }



    if(room_type == 1 && pref_type == "PP" && part.length == 2) {

        //alert('pp'); return false;

        if(selected_paid.length == 0){
            alert("Add payment for at least one participant");
            return false;
        }

        var data = {
            'btnReservePartnerSubmit': true,
            'pp_select': pp_select.val(),
            'pref_type': pref_type,
            'room_type': room_type,
            'participation': participation
        }


        var url = site_url + "booking/reserve_partner?part_id="+encodeURIComponent(participant);
        console.log(url);
        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: data,
            async: true,
            beforeSend: function () {
                blockUI();
            },
            success: function (data) { //alert(data.status); return false;
                if(data == 411){ __auth(); return false; }
                unblockUI();


                if(data.status == 3){
                    alert("Please add the remaining participant!");
                    return false;
                }


                if(data.status == 4){
                    alert("Participant already booked a room\nRoom number is #: "+data.room_number);
                    return false;
                }

                if(data.status == 5){
                    alert("Can not find partner payment & reservation details!");
                    return false;
                }


                if(data.status == 6){
                    alert("Partner must be opposite sex!");
                    return false;
                }

                if(data.status == 7){
                    alert("Please sign up participant!");
                    return false;
                }

                if(data.status == 8){
                    alert("No payment found for participant!");
                    return false;
                }


                if(data.status == 1){

                    defaultModal({
                        "path": 'assigned-rooms/details/'+data.room+'/'+event_id,
                        "title": "Assigned Room Details"
                    });


                } else {
                    alert('An error occurred, please try again!');
                    return false;
                }


            },
            error: function () {
                unblockUI();
                alert('An error occurred, please try again!');
            }
        });

        return false;


    } else if(room_type == 1 && !pref_type && part.length == 1) {

        //alert('one man'); return false;
        //### ADD ONE MAN

        var data = {
            'btnReserveOneManSubmit': true,
            'pref_type': pref_type,
            'room_type': room_type,
            'participation': participation
        }

        var url = site_url + "booking/reserve_one_man?part_id="+encodeURIComponent(participant);
        console.log(url);
        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: data,
            async: true,
            beforeSend: function () {
                blockUI();
            },
            success: function (data) { //alert(data.status); return false;
                if(data == 411){ __auth(); return false; }
                unblockUI();

                if(data.status == 3){
                    alert("Please add the remaining participant!");
                    return false;
                }

                if(data.status == 4){
                    alert("Participant already booked a room\nRoom number is #: "+data.room_number);
                    return false;
                }

                /*
                if(data.status == 5){
                    alert("Can not find partner payment & reservation details!");
                    return false;
                }
                */

                if(data.status == 8){
                    alert("No payment found for participant!");
                    return false;
                }

                if(data.status == 7){
                    alert("Please sign up participant!");
                    return false;
                }

                if(data.status == 1){

                    defaultModal({
                        "path": 'assigned-rooms/details/'+data.room+'/'+event_id,
                        "title": "Assigned Room Details"
                    });


                } else {
                    alert('An error occurred, please try again!');
                    return false;
                }


            },
            error: function () {
                unblockUI();
                alert('An error occurred, please try again!');
            }
        });

        return false;
    } else if(room_type == 2) {

        //##### 2 MAN PREFERENCE

        if(!pref_type){
            alert('Please select preference type!');
            return false;
        }

        if(part.length > 2){
            alert('You cannot select more than 2 participants!');
            return false;
        }

        if(part.length != selected_paid.length){
            alert('Please add payment for all partners');
            return false;
        }


        var data = {
            'btnReserveTwoManSubmit': true,
            'pref_type': pref_type,
            'room_type': room_type,
            'participation': participation
        }

        var url = site_url + "booking/reserve_two_man?part_id="+encodeURIComponent(participant);
        console.log(url);
        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: data,
            async: true,
            beforeSend: function () {
                blockUI();
            },
            success: function (data) { //alert(data.status); return false;
                if(data == 411){ __auth(); return false; }
                unblockUI();


                if(data.status == 7){
                    alert("Please sign up participant!");
                    return false;
                }


                if(data.status == 6){
                    alert("Sharing participants cannot be opposite sex!");
                    return false;
                }

                if(data.status == 10){
                    alert('Please add payment for all partners');
                    return false;
                }


                if(data.status == 11){
                    alert('Please select same preference for pairing!');
                    return false;
                }


                if(data.status == 4){
                    alert("Participants already booked rooms\nRoom numbers #: "+data.room_number);
                    return false;
                }


                /*


                if(data.status == 5){
                    alert("Can not find partner payment & reservation details!");
                    return false;
                }
                */

                if(data.status == 1){

                    defaultModal({
                        "path": 'assigned-rooms/details/'+data.room+'/'+event_id,
                        "title": "Assigned Room Details"
                    });


                } else {
                    alert('An error occurred, please try again!');
                    return false;
                }


            },
            error: function () {
                unblockUI();
                alert('An error occurred, please try again!');
            }
        });

    } else if(room_type == 3) {

        //##### 3 MAN PREFERENCE

        if(!pref_type){
            alert('Please select preference type!');
            return false;
        }

        if(part.length != selected_paid.length){
            alert('Please add payment for all partners');
            return false;
        }


        var data = {
            'btnReserveThreeManSubmit': true,
            'pref_type': pref_type,
            'room_type': room_type,
            'participation': participation
        }

        var url = site_url + "booking/reserve_three_man?part_id="+encodeURIComponent(participant);
        console.log(url);
        $.ajaxSetup({cache: false});
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            data: data,
            async: true,
            beforeSend: function () {
                blockUI();
            },
            success: function (data) { //alert(data.status); return false;
                if(data == 411){ __auth(); return false; }
                unblockUI();


                if(data.status == 7){
                    alert("Please sign up participant!");
                    return false;
                }


                if(data.status == 6){
                    alert("Sharing participants cannot be opposite sex!");
                    return false;
                }

                if(data.status == 10){
                    alert('Please add payment for all partners');
                    return false;
                }

                if(data.status == 11){
                    alert('Please select same preference for pairing!');
                    return false;
                }

                if(data.status == 4){
                    alert("Participants already booked rooms\nRoom numbers #: "+data.room_number);
                    return false;
                }


                /*


                 if(data.status == 5){
                 alert("Can not find partner payment & reservation details!");
                 return false;
                 }
                 */

                if(data.status == 1){

                    defaultModal({
                        "path": 'assigned-rooms/details/'+data.room+'/'+event_id,
                        "title": "Assigned Room Details"
                    });


                } else {
                    alert('An error occurred, please try again!');
                    return false;
                }


            },
            error: function () {
                unblockUI();
                alert('An error occurred, please try again!');
            }
        });

    }


}





function reAssignSubmit() {
    var room_type = $("[name=room_type]").val();
    var payment_mode = $("[name=payment_mode]").val();
    var trans_id = $("[name=trans_id]").val();
    var comment = $("[name=comment]").val();
    var amount_paid = $("[name=amount_paid]").val();

    var participant = [];
    $('.booking-checked').each(function() {
        participant.push($(this).val());
    });

    if(!room_type){
        alert("Please select room type!");
        $("[name=room_type]").focus();
        return false;
    }

    if(participant.length != room_type){
        alert("Please select right number of participant to reassign!");
        $("[name=room_type]").focus();
        return false;
    }

    var pref_type = '';
    var friend_share_type = '';
    var friend_id = '';
    //var partner_id = '';

    pref_type = $("[name=pref_type]:checked").val();
    if($("#PP").is(":checked")){

        if(participant.length != 2){
            alert("Please select partner!");
            return false;
        }

        pref_type = "PP";
    } else if (pref_type == "N" || pref_type == "P") {
        friend_share_type = $("[name=friend_share_type]:checked").val();
        if (friend_share_type == 2) {
            friend_id = $("#friend_id").val();

            if(!friend_id){
                alert('Participant ID is required!');
                $("#friend_id").focus();
                return false;
            }
        }

        if (pref_type == "N"){
            friend_share_type = '';
        }

    } else {
        pref_type = '';
    }

    var data = {
        'btnReassignSubmit': true,
        'pref_type': pref_type,
        'room_type': room_type
    }


    var url = site_url + "participant/reassign?part_id="+encodeURIComponent(participant);
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () { //alert(1);
            blockUI();
        },
        success: function (data) {
            if(data == 411){ __auth(); return false; }
            unblockUI();
            //alert(data.status); return false;

            /*
             if(data.status == 2){
             alert("Participant not found!\n" +
             "Please note that your friend must register to use His/Her ID!");
             $("#friend_id").focus();
             return false;
             }

             */


            if(data.status == 3){
                alert("Please add the remaining participant!");
                return false;
            }


            if(data.status == 4){
                alert("Participant have already booked a room!");
                return false;
            }

            if(data.status == 5){
                alert("You can not share with opposite sex!");
                return false;
            }

            /*
             if(data.status == 6){
             alert("Participant have already booked a room!");
             $("#friend_id").focus();
             return false;
             }


             if(data.status == 7){
             alert("Your friend selected a different room preference!");
             $("#friend_id").focus();
             return false;
             }
             */

            //$(".modal-body").html(data.summary);


            if(data.status == 1){

                defaultModal({
                    "path": 'assigned-rooms/details/'+data.room+'/'+event_id,
                    "title": "Assigned Room Details"
                });

                /*
                $("#search_p").val('');
                $("ul.booking-added, ul.search-results").html("");
                $(".booking-actions").hide();
                alert('Reservation was successful!');
                return false;
*/

            } else {
                alert('An error occurred, please try again!');
                return false;
            }


        },
        error: function () {
            unblockUI();
            alert('An error occurred, please try again!');
        }
    });
}





function openRefundOption(transId){
    $("#page"+transId+" .this-refund-transaction").popover('destroy');
    $("#page"+transId+" .this-refund-transaction").popover({
        html: true,
        'placement': 'top',
        title: function () {
            return $(this).parent().find('.head').html();
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    }).popover('show');
}





function openQuickDetails(id, position){ //alert(id);
    $(".part-"+id).popover('destroy');

    $(".part-"+id).popover({
        html: true,
        'placement': 'top',
        title: function () {
            return $(this).parent().find('.head').html();
        },
        content: function () {
            return $(this).parent().find('.content').html();
        }
    }).popover('show');
}




function createUserAccount(){
    var name = $("#name").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var level = $("#level").val();

    if(!name){
        alert('Full name is required!');
        $("#name").focus();
        return false;
    }

    if(!username){
        alert('username is required!');
        $("#username").focus();
        return false;
    }

    if(!password){
        alert('Password is required!');
        $("#password").focus();
        return false;
    }

    if(!level){
        alert('Account level is required!');
        $("#level").focus();
        return false;
    }

    var data = {
        'btnAdd': true,
        'name': name,
        'username': username,
        'password': password,
        'level': level
    }

    var url = site_url + 'user-account/add-new';

    $.ajaxSetup ({ cache: false });
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function() {
            blockUI();
        },
        success: function(data) {
            if(data == 411){ __auth(); return false; }
            unblockUI();
            if(data.status == 1) {
                pageData({
                    'path' : 'user-account/home',
                    'attached' : 'page-data'
                });
                closeModal();
                alert("Account was successfully created!");
            } else if(data.status == 2){
                alert('Username already exist');
            } else {
                alert('An error occurred, please re-try!');
            }
        },
        error: function() {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}


function assignRoomKey(id){
    var key_id = $("#key_id").val();

    if(!key_id){
        alert('All fields are required!');
        $("#contact").focus();
        return false;
    }

    var data = {
        'btnAdd': true,
        'key_id': key_id,
        'id': id
    }

    var url = site_url + 'assigned-rooms/assign-key/';
    console.log(url);

    $.ajaxSetup ({ cache: false });
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function() {
            blockUI();
        },
        success: function(data) {
            if(data == 411){ __auth(); return false; }
            unblockUI();

            if(data.status == 1) {
                $(".tr-"+id+" .cell-3").html(key_id.toUpperCase());
                closeModal();
                alert('Room Key successfully added');
            } else {
                alert('An error occurred, please re-try!');
            }
        },
        error: function() {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}





function checkInStatus(unique_id, status){
    conf = confirm('Are you sure you want to change check in status');

    if(!conf){
        return false;
    }

    var data = {
        'btnCheckIn': true,
        'unique_id': unique_id,
        'status': status
    }

    var url = site_url + 'assigned-participants';
    //console.log(url);

    $.ajaxSetup ({ cache: false });
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function() {
            blockUI();
            //console.log(id);
        },
        success: function(data) {
            unblockUI();

            if(data.status == 1){

                if(status == 1){
                    var state = '<span class="green font-bold" ' +
                    'onclick="checkInStatus(\''+unique_id+'\', 0)">CHECKED IN</span>';
                } else {
                    var state = '<span class="gray" ' +
                        'onclick="checkInStatus(\''+unique_id+'\', 1)">NOT CONFIRMED</span>';
                }

                $(".tr-"+unique_id+" .cell-7").html(state);
            } else {
                alert('An error occurred, please re-try!');
            }
        },
        error: function() {
            alert('An error occurred, please re-try!');
            unblockUI();
        }
    });

}




function deleteReservation(unique_id, room_number){
    var conf = confirm('Are you sure you want to remove this reservation!');
    if(!conf){
        return false;
    }

    var current_search = $("#search_p").val();

    var data = {
        'btnDeleteReservation': true,
        'unique_id': unique_id,
        'room_number': room_number
    }

    var url = site_url + 'assigned-rooms';
    console.log(url);

    $.ajaxSetup ({ cache: false });
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function() {
            blockUI();
            //console.log(id);
        },
        success: function(data) {
            inlineUnblock("inline-popover");
            if(data.status == 1){
                alert('Reservation successfully removed!');
                $( "ul.list-selected-booking li.unique-id-"+unique_id )
                    .load( site_url + 'booking/details?details='+unique_id );
                getBookingSearch(room_number);

                unblockUI();
            } else {
                alert('An error occurred, please re-try!');
            }
        },
        error: function() {
            alert('An error occurred, please re-try!');
            unblockUI();
        }
    });
}




function addPastor() {
    var name = $("#name").val();
    var phone = $("#phone").val();
    var location = $("#location").val();

    if (!name || !phone || !location) {
        alert('All fields are required!');
        $("#name").focus();
        return false;
    }

    var data = {
        'btnAddPastor': true,
        'name': name,
        'phone': phone,
        'location': location
    }

    var url = site_url + 'pastors/add-new';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'pastors/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}


function editPastor(id) {
    var name = $("#name").val();
    var phone = $("#phone").val();
    var location = $("#location").val();

    if (!name || !phone || !location) {
        alert('All fields are required!');
        $("#name").focus();
        return false;
    }

    var data = {
        'btnEditPastor': true,
        'id': id,
        'name': name,
        'phone': phone,
        'location': location
    }

    var url = site_url + 'pastors/edit';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'pastors/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function deletePastor(id) {

    conf = confirm('Are you sure you want to delete selected pastor?');

    if(!conf){
        return false;
    }

    var data = {
        'btnDeletePastor': true,
        'id': id
    }

    var url = site_url + 'pastors/delete';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'pastors/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}


function generateOneOnOne(){
    var pastors = [];
    var allocated = [];

    var pastor_id = $("[name=pastor_id]");
    var one_percent = $("[name=one_percent]");
    var sumAllocation = 0;

    $(one_percent).each(function() {
        if($(this).val() !='' && !isNaN($(this).val())){
            allocated.push($(this).val());
            sumAllocation += parseInt($(this).val());
        }
    });

    $(pastor_id).each(function() {
        pastors.push($(this).val());
    });




    if(pastors.length != allocated.length){
        alert("Please add all allocations!");
        return false;
    }

    if(parseInt(sumAllocation) != 100){
        alert("All allocations must be equal to 100!");
        return false;
    }


    var data = {
        'btnAllocatePastor': true
    }

    var url = site_url + 'one-on-one/add-new?pastors_id='+
        encodeURIComponent(pastors)+'&allocated='+encodeURIComponent(allocated);
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) { //alert(data.status); return false;

            if (data.status == 2) {
                alert("No enough participant!");
            } else if (data.status == 1) {
                pageData({
                    'path' : 'one-on-one/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });

}





function editOneOnOne(id) {
    var pastor = $("#pastor").val();

    if (!pastor) {
        alert('Fullname and department are required!');
        $("#pastor").focus();
        return false;
    }

    var data = {
        'btnEditOneOnOne': true,
        'id': id,
        'pastor': pastor
    }

    var url = site_url + 'one-on-one/edit';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'one-on-one/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}






function addUser() {
    var name = $("#name").val();
    var username = $("#username").val();
    var password = $("#password").val();
    var level = $("#level").val();

    if (!name || !username || !password || !level) {
        alert('All fields are required!');
        $("#name").focus();
        return false;
    }

    var data = {
        'btnAddUser': true,
        'name': name,
        'username': username,
        'password': password,
        'level': level
    }

    var url = site_url + 'user-account/add-new';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 2) {
                alert("User already exist!");
            } else if (data.status == 1) {
                pageData({
                    'path' : 'user-account/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}


function editUser(id) {
    var name = $("#name").val();
    var password = $("#password").val();
    var level = $("#level").val();
    var status = $("#status").val();

    if (!name || !level || !status) {
        alert('All fields are required!');
        $("#name").focus();
        return false;
    }

    var data = {
        'btnEditUser': true,
        'id': id,
        'name': name,
        'password': password,
        'level': level,
        'status': status
    }

    var url = site_url + 'user-account/edit';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) { //alert(data.status); return false;
            if (data.status == 2) {
                alert("User already exist!");
            } else if (data.status == 1) {
                pageData({
                    'path' : 'user-account/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function deleteUser(id) {

    conf = confirm('Are you sure you want to delete selected pastor?');

    if(!conf){
        return false;
    }

    var data = {
        'btnDeletePastor': true,
        'id': id
    }

    var url = site_url + 'pastors/delete';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'pastors/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}


function checkInParticipant(room_number){
    var participants = [];

    $('[name=selected_check_in]:checked').each(function() {
        participants.push($(this).val());
    });

    if(participants.length == 0){
        alert("Please select participants!");
        return false;
    }

    var data = {
        'btnCheckInSelected': true
    }

    var url = site_url + 'assigned-rooms?participants='+encodeURIComponent(participants);
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) { //alert(data.status); return false;
            if (data.status == 1) {
                showRoomDetails(room_number);
                alert('Selected participants successfully checked in!');
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });

}


function addRoomKeys() {
    var keys = [];

    $('[name=room_key_id]').each(function() {
        if($(this).val() !=''){
            keys.push($(this).val());
        }
    });

    if(keys.length == 0){
        alert("Please add room keys!");
        return false;
    }

    var data = {
        'btnAddRoomKeys': true
    }

    var url = site_url + 'room-keys/add-new?room_key_ids='+encodeURIComponent(keys);
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'room-keys/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function editRoomKey(id) {
    var key = $("[name=room_key_id]").val();

    if (!key) {
        alert('Room key ID is required!');
        $("[name=room_key_id]").focus();
        return false;
    }

    var data = {
        'btnEditRoomKey': true,
        'id': id,
        'key': key
    }

    var url = site_url + 'room-keys/edit';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) { //alert(data.status); return false;
            if (data.status == 1) {
                pageData({
                    'path' : 'room-keys/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}




function deleteRoomKey(id) {

    conf = confirm('Are you sure you want to delete selected key?');

    if(!conf){
        return false;
    }

    var data = {
        'btnDeleteRoomKey': true,
        'id': id
    }

    var url = site_url + 'room-keys/delete';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'room-keys/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function assignKey(room_number, room_key_id, location){
    var data = {
        'btnAssignRoomKey': true,
        'room_number': room_number,
        'room_key_id': room_key_id
    }

    var url = site_url + 'assigned-rooms/assign-key';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {

                if(location == 1){
                    showRoomDetails(room_number);
                    alert('Room key successfully assigned!');
                    //closeModal();
                } else {
                    pageData({
                        'path': 'assigned-rooms/home',
                        'attached': 'page-data'
                    });
                    closeModal();
                }

            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function deleteAllData(id) {
    conf = confirm('Participant data will be delete. Click Ok to continue!');
    if(!conf){
        return false;
    }

    var data = {
        'btnDeleteAllData': true,
        'id': id
    }

    var url = site_url + 'participant/home';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'participant/home/2',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}









function addWorkers() {
    var name = $("#name").val();
    var phone = $("#phone").val();
    var department = $("#department").val();

    if (!name || !department) {
        alert('Fullname and department are required!');
        $("#name").focus();
        return false;
    }

    var data = {
        'btnAddWorkers': true,
        'name': name,
        'phone': phone,
        'department': department
    }

    var url = site_url + 'workers/add-new';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'workers/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}


function editWorkers(id) {
    var name = $("#name").val();
    var phone = $("#phone").val();
    var department = $("#department").val();

    if (!name || !department) {
        alert('Fullname and department are required!');
        $("#name").focus();
        return false;
    }

    var data = {
        'btnEditWorkers': true,
        'id': id,
        'name': name,
        'phone': phone,
        'department': department
    }

    var url = site_url + 'workers/edit';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'workers/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}



function deleteWorkers(id) {

    conf = confirm('Are you sure you want to delete selected worker?');

    if(!conf){
        return false;
    }

    var data = {
        'btnDeleteWorkers': true,
        'id': id
    }

    var url = site_url + 'workers/delete';
    console.log(url);
    $.ajaxSetup({cache: false});
    $.ajax({
        url: url,
        type: "POST",
        dataType: "json",
        data: data,
        async: true,
        beforeSend: function () {
            blockUI();
        },
        success: function (data) {
            if (data.status == 1) {
                pageData({
                    'path' : 'workers/home',
                    'attached' : 'page-data'
                });
                closeModal();
            } else {
                alert('An error occurred, please re-try!');
            }
            unblockUI();
        },
        error: function () {
            unblockUI();
            alert('An error occurred, please re-try!');
        }
    });
}
